$(document).ready(function() {
	$("#add_new_img_input").on("click", function() {
		var parent_tag = $(this).parent().parent();
		var parent_to_add = $(parent_tag).find(".images_block_inputs:first");
		var input_parent = $(parent_to_add).find(".fileinput").children(":first");
		var inputs_amount = $(parent_tag).find(".images_block_inputs").length;
		var new_input_id = inputs_amount + 1;
		var string_to_add_left = '<div class="images_block_inputs"><div class="col-sm-3 col-xs-3 col-md-3 col-xs-3 col-md-3"></div><div class="fileinput fileinput-new" data-provides="fileinput"><span class="btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="img_';
        var string_to_add_right = '"></input></span><span class="fileinput-filename"></span><a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a></div><br></div>';
        $(parent_to_add).append(string_to_add_left + new_input_id + string_to_add_right);
	});
});