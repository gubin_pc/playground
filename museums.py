from pymongo import MongoClient
from bson.objectid import ObjectId
from gridfs import GridFS
from flask import Flask, render_template, jsonify, redirect, url_for, request, send_file, make_response
import uuid
from qrcode import *
import base64
import StringIO

app = Flask(__name__)
app.config.from_object(__name__)

ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg', 'gif']

@app.route('/')
def start():
    guid = get_guid()
    return render_template('index.html', guid=guid)

@app.route('/qr/<guid>')
def serve_img(guid):
    qr = QRCode(version=1)
    qr.add_data(guid)
    qr.make()
    im = qr.make_image()
    return serve_pil_image(im)

@app.route('/add_entry', methods = ['POST'])
def add_entry():
    if request.method == 'POST':
        client = MongoClient()
        db = client["museums"]
        objects = client["museums"]["objects"]
        fs = GridFS(db)
        
        files = request.files
        img_ids = []
        _object = {}

        for img in files:
            file_type = files[img].content_type
            file_body = files[img]
            file_name = files[img].filename
            if file_body and allowed_file(file_body.filename):
                img_ids.append(fs.put(file_body, content_type=file_type, filename=file_name))


        for key in request.form:
            if key == 'guid_code':
                _object['_id'] = request.form[key]
                continue
            if request.form[key]:    
                _object[key] = request.form[key]
        
        _object["images"] = img_ids
        objects.save(_object)
        return redirect(url_for('thanks'))

@app.route('/update_entry', methods = ['POST'])
def update_entry():
    if request.method == 'POST':
        client = MongoClient()
        db = client["museums"]
        objects = client["museums"]["objects"]
        fs = GridFS(db)
        files = request.files
        img_ids = []

        query = {"_id":(request.form["guid_code"]).encode('utf-8')}
        _object = objects.find_one(query)
        if _object:
            img_ids = list(_object['images'])
            for key in img_ids:
                if str(key) in request.form:
                    img_ids.remove(key)
                    fs.delete(key)

        for img in files:
            file_type = files[img].content_type
            file_body = files[img]
            file_name = files[img].filename
            if file_body and allowed_file(file_body.filename):
                img_ids.append(fs.put(file_body, content_type=file_type, filename=file_name))

        for key in request.form:
            if key == 'guid_code':
                _object['_id'] = request.form[key]
                continue
            if key in [str(i) for i in _object['images']]:
                continue
            if request.form[key]:    
                _object[key] = request.form[key]
        
        _object["images"] = img_ids
        objects.save(_object)
        return redirect(url_for('thanks'))

@app.route('/edit', methods = ['POST'])
def edit_entry():
    if request.method == 'POST':
        client = MongoClient()
        objects = client["museums"]["objects"]
        query = {"inventory_number":(request.form["inventory_number"]).encode('utf-8')}
        _object = objects.find_one(query)
        if _object:
            file_list = []
            for item in _object['images']:
                file_list.append(url_for('serve_gridfs_file', oid=item))
            files = zip(_object['images'], file_list)
            _object['files'] = files
            return render_template('edit.html', **_object)
        else:
            return render_template('search.html', show_error=True)

@app.route('/files/<oid>')
def serve_gridfs_file(oid):
    client = MongoClient()        
    db = client.museums
    fs = GridFS(db)
    file = fs.get(ObjectId(oid))
    response = make_response(file.read())
    response.mimetype = file.content_type
    return response

@app.route('/search')
def search():
    return render_template('search.html', show_error=False)

@app.route('/thanks')
def thanks():
    return render_template('thanks.html')

def get_guid():
    guid = base64.urlsafe_b64encode(uuid.uuid4().bytes)
    return guid.replace('=', '')

def serve_pil_image(pil_img):
    img_io = StringIO.StringIO()
    pil_img.save(img_io)
    img_io.seek(0)
    return send_file(img_io, mimetype='image/jpeg')

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

if __name__ == '__main__':
    app.run(debug=True)
